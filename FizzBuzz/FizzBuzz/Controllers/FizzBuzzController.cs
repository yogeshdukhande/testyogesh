﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzController.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using FizzBuzz.Common;
    using FizzBuzz.Model;
    using FizzBuzz.Models;

    /// <summary>
    /// Definition of FizzBuzzController class
    /// </summary>
    public class FizzBuzzController : Controller
    {
        /// <summary>
        /// Define IStrategic
        /// </summary>
        private readonly IStrategic businessLogic;

        /// <summary>
        /// Initializes a new instance of the FizzBuzzController class.
        /// </summary>
        /// <param name="inputbusinessLogic">The input businessLogic</param>
        public FizzBuzzController(IStrategic inputbusinessLogic)
        {
            this.businessLogic = inputbusinessLogic;
        }

        /// <summary>
        /// Action method to render the initial view.
        /// </summary>
        /// <returns>FizzBuzzView View</returns>
        [HttpGet]
        public ActionResult FizzBuzzView()
        {
            return this.View("FizzBuzzView", new FizzBuzzModel());
        }

        /// <summary>
        /// Action method to render on Get Data button submit.
        /// </summary>
        /// <param name="inputpostModel">the input postModel</param>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "DisplayList")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzView(FizzBuzzModel inputpostModel)
        {
            FizzBuzzModel objData = new FizzBuzzModel() { Number = inputpostModel.Number };
            objData.Page.PageIndex = 1;

            if (ModelState.IsValid)
            {
                this.FillNumberList(objData);
            }

            this.TempData["FizzBuzzPage"] = objData.Page;
            return this.View("FizzBuzzView", objData);
        }

        /// <summary>
        /// Action method to render on Previous button click.
        /// </summary>
        /// <param name="inputpostModel">the input postModel</param>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "Previous")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzViewPrevious(FizzBuzzModel inputpostModel)
        {
            FizzBuzzModel objData = new FizzBuzzModel() { Number = inputpostModel.Number };
            objData.Page = (this.TempData["FizzBuzzPage"] != null) ? TempData["FizzBuzzPage"] as Paging : new Paging();
            objData.Page.PageIndex = (objData.Page.PageIndex == 1) ? objData.Page.PageIndex : (objData.Page.PageIndex - 1);

            if (ModelState.IsValid)
            {
                this.FillNumberList(objData);
            }

            this.TempData["FizzBuzzPage"] = objData.Page;
            return this.View("FizzBuzzView", objData);
        }

        /// <summary>
        /// Action method to render on Next button click.
        /// </summary>
        /// <param name="inputpostModel">the input postModel</param>
        /// <returns>FizzBuzzView View</returns>
        [HttpPost]
        [Button(ButtonName = "Next")]
        [ActionName("FizzBuzzView")]
        public ActionResult FizzBuzzViewNext(FizzBuzzModel inputpostModel)
        {
            FizzBuzzModel objData = new FizzBuzzModel() { Number = inputpostModel.Number };
            objData.Page = (this.TempData["FizzBuzzPage"] != null) ? TempData["FizzBuzzPage"] as Paging : new Paging();
            objData.Page.PageIndex = (objData.Page.PageIndex == objData.Page.PageCount) ? objData.Page.PageIndex : (objData.Page.PageIndex + 1);

            if (ModelState.IsValid)
            {
                this.FillNumberList(objData);
            }

            this.TempData["FizzBuzzPage"] = objData.Page;
            return this.View("FizzBuzzView", objData);
        }

        /// <summary>
        /// fill number list
        /// </summary>
        /// <param name="inputData">input InputData</param>
        private void FillNumberList(FizzBuzzModel inputData)
        {
            inputData.DivisionList = this.businessLogic.GetList(inputData.Number.Value, System.DateTime.Now.DayOfWeek);
            var startndex = (inputData.Page.PageIndex - 1) * FizzBuzzModel.PageSize;
            inputData.DivisionList = inputData.DivisionList.GetRange(startndex, inputData.Number.Value - startndex < FizzBuzzModel.PageSize ? inputData.Number.Value - startndex : FizzBuzzModel.PageSize);
        }
    }
}
