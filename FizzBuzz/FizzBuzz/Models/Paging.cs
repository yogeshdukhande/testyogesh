﻿//-----------------------------------------------------------------------
// <copyright file="Paging.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;

    /// <summary>
    /// Paging for have current pageIndex and PageCount
    /// </summary>
    public class Paging
    {
        /// <summary>
        /// Initializes a new instance of the Paging class.
        /// </summary>
        public Paging()
        {
            this.PageIndex = 1;
        }

        /// <summary>
        /// Gets or sets PageIndex
        /// </summary>
        public int PageIndex { get; set; }

        /// <summary>
        /// Gets or sets PageCount
        /// </summary>
        public int PageCount { get; set; }
    }
}