﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzModel.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Web;
    using FizzBuzz.Model;
    using FizzBuzzBusinessLayer;

    /// <summary>
    /// The view model for the Fizz Buzz Operation
    /// </summary>
    public class FizzBuzzModel
    {
        /// <summary>
        /// Default value of Page Size
        /// </summary>
        public const int PageSize = 20;

        /// <summary>
        /// Define nNumber
        /// </summary>
        private int? inputNumber;

        /// <summary>
        /// Initializes a new instance of the FizzBuzzModel class.
        /// </summary>
        public FizzBuzzModel()
        {
            this.Page = new Paging();
        }

        /// <summary>
        /// Gets or sets Page
        /// </summary>
        public Paging Page { get; set; }

        /// <summary>
        /// Gets or sets the input number.
        /// </summary>
        [Required]
        [Display(Name = "Please enter the number")]
        [Range(1, 1000, ErrorMessage = "Please enter a value between 1 and 1000")]
        public int? Number
        {
            get
            {
                return this.inputNumber;
            }

            set
            {
                this.inputNumber = value;
                this.Page.PageIndex = 1;
                this.Page.PageCount = Convert.ToInt32(this.inputNumber) / PageSize;
                this.Page.PageCount = (this.inputNumber % PageSize) == 0 ? this.Page.PageCount : this.Page.PageCount + 1;
            }
        }

        /// <summary>
        /// Gets or sets the Division object list.
        /// </summary>
        public List<IFizzBuzz> DivisionList { get; set; }
    }
}