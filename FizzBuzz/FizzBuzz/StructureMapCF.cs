﻿//-----------------------------------------------------------------------
// <copyright file="StructureMapCF.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Mvc;
    using System.Web.Routing;
    using StructureMap;

    /// <summary>
    /// StructureMap controller factory
    /// </summary>
    public class StructureMapCF : DefaultControllerFactory
    {
        /// <summary>
        /// Gets the controller instance
        /// </summary>
        /// <param name="requestContext">request context</param>
        /// <param name="controllerType">controller type</param>
        /// <returns> return IController</returns>
        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                return base.GetControllerInstance(requestContext, controllerType);
            }

            return ObjectFactory.GetInstance(controllerType) as Controller;
        }
    }
}