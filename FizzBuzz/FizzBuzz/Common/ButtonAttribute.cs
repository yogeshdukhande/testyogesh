﻿//-----------------------------------------------------------------------
// <copyright file="ButtonAttribute.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Common
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;
    using System.Web;
    using System.Web.Mvc;

    /// <summary>
    /// Definition of ButtonAttribute class
    /// </summary>
    public class ButtonAttribute : ActionMethodSelectorAttribute
    {
        /// <summary>
        /// Gets or sets ButtonName
        /// </summary>
        public string ButtonName { get; set; }

        /// <summary>
        /// definition of IsValidForRequest
        /// </summary>
        /// <param name="controllerContext">input controllerContext</param>
        /// <param name="methodInfo">input methodInfo</param>
        /// <returns>return boolean</returns>
        public override bool IsValidForRequest(ControllerContext controllerContext, MethodInfo methodInfo)
        {
            return controllerContext.Controller.ValueProvider.GetValue(this.ButtonName) != null;
        }
    }
}