﻿//-----------------------------------------------------------------------
// <copyright file="Global.asax.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------
namespace FizzBuzz
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;
    using System.Web.Optimization;
    using System.Web.Routing;
    using FizzBuzz.Model;
    using StructureMap;
    //// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801

    /// <summary>
    /// Definition of MVCApplication class
    /// </summary>
    public class MVCApplication : System.Web.HttpApplication
    {
        /// <summary>
        /// application start event
        /// </summary>
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();

            WebAPIConfig.Register(GlobalConfiguration.Configuration);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            Initialize_Structuremap();
            ControllerBuilder.Current.SetControllerFactory(new StructureMapCF());
        }

        /// <summary>
        /// initialize structureMap
        /// </summary>
        private static void Initialize_Structuremap()
        {
            ObjectFactory.Initialize(m =>
            {
                m.For<IStrategic>().Use<FizzBuzzStrategic>();
            });
        }
    }
}