﻿//-----------------------------------------------------------------------
// <copyright file="SimpleDivisionTests.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzz.Model;
    using FizzBuzzBusinessLayer;
    using NUnit.Framework;

    /// <summary>
    /// Definition of SimpleDivisionTests class
    /// </summary>
    [TestFixture]
    public class SimpleDivisionTests
    {
        /// <summary>
        /// count of numbers should be same as input number
        /// </summary>
        [Test]
        public void Input_Number_24_Should_Have_24_NumbersCount()
        {
            IStrategic strategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = strategicLogic.GetList(24, System.DateTime.Now.DayOfWeek);
            Assert.AreEqual(24, list.Count);
            Assert.AreEqual("1", ((IFizzBuzz)list[0]).Message);
            Assert.AreEqual("2", ((IFizzBuzz)list[1]).Message);
        }
    }
}
