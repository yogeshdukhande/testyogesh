﻿//-----------------------------------------------------------------------
// <copyright file="FizzTests.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzz.Model;
    using FizzBuzzBusinessLayer;
    using NUnit.Framework;

    /// <summary>
    /// Definition of FizzTests Class
    /// </summary>
    [TestFixture]
    public class FizzTests
    {
        /// <summary>
        /// if number is divisible by 3 then replace with proper message
        /// </summary>
        [Test]
        public void Input_Number_Replace_3_Divisible_With_Proper_Message_Color()
        {
            IStrategic strategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = strategicLogic.GetList(24, System.DateTime.Now.DayOfWeek);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wizz", ((IFizzBuzz)list[2]).Message);
                Assert.AreEqual("Wizz", ((IFizzBuzz)list[5]).Message);
            }
            else
            {
                Assert.AreEqual("Fizz", ((IFizzBuzz)list[2]).Message);
                Assert.AreEqual("Fizz", ((IFizzBuzz)list[5]).Message);
            }

            Assert.AreEqual("blue", ((IFizzBuzz)list[2]).MessageColor);
            Assert.AreEqual("blue", ((IFizzBuzz)list[5]).MessageColor);
        }
    }
}
