﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzControllerTests.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Web.Mvc;
    using FizzBuzz.Controllers;
    using FizzBuzz.Model;
    using FizzBuzz.Models;
    using FizzBuzzBusinessLayer;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Definition of FizzBuzzControllerTests class
    /// </summary>
    [TestFixture]
    public class FizzBuzzControllerTests
    {
        /// <summary>
        /// show fizzBuzz view
        /// </summary>
        [Test]
        public void Should_Give_FizzBuzzView_On_InputRequest()
        {
            Mock<IStrategic> strategicLogic = new Mock<IStrategic>();
            FizzBuzzController controllerToTest = new FizzBuzzController(strategicLogic.Object);
            var resultView = controllerToTest.FizzBuzzView() as ViewResult;
            Assert.AreEqual("FizzBuzzView", resultView.ViewName);
        }

        /// <summary>
        /// show 20 items on page
        /// </summary>
        [Test]
        public void Should_Show__20_Items_On_One_Page()
        {
            Mock<IStrategic> strategicLogic = new Mock<IStrategic>();
            strategicLogic.Setup(m => m.GetList(21, System.DateTime.Now.DayOfWeek)).Returns(this.GetFizzBuzzList(21));
            FizzBuzzController controller = new FizzBuzzController(strategicLogic.Object);
            var result = controller.FizzBuzzView(new FizzBuzzModel { Number = 21 }) as ViewResult;
            var viewModel = (FizzBuzzModel)result.Model;
            Assert.AreEqual(20, viewModel.DivisionList.Count);
        }

        /// <summary>
        /// should implement PageCount
        /// </summary>
        [Test]
        public void Should_implement_PageCount()
        {
            Mock<IStrategic> strategicLogic = new Mock<IStrategic>();
            strategicLogic.Setup(m => m.GetList(63, System.DateTime.Now.DayOfWeek)).Returns(this.GetFizzBuzzList(63));
            FizzBuzzController controller = new FizzBuzzController(strategicLogic.Object);
            var result = controller.FizzBuzzView(new FizzBuzzModel { Number = 63 }) as ViewResult;
            var viewModel = (FizzBuzzModel)result.Model;
            Assert.AreEqual(4, viewModel.Page.PageCount);
        }

        /// <summary>
        /// should implement paging
        /// </summary>
        [Test]
        public void Should_implement_paging()
        {
            Mock<IStrategic> strategicLogic = new Mock<IStrategic>();
            strategicLogic.Setup(m => m.GetList(21, System.DateTime.Now.DayOfWeek)).Returns(this.GetFizzBuzzList(21));
            FizzBuzzController controller = new FizzBuzzController(strategicLogic.Object);
            var result = controller.FizzBuzzViewNext(new FizzBuzzModel { Page = new Paging() { PageIndex = 2 }, Number = 21 }) as ViewResult;
            var viewModel = (FizzBuzzModel)result.Model;
            Assert.AreEqual(1, viewModel.DivisionList.Count); ////Second page shouold have ony 1 Value
        }

        /// <summary>
        /// should return List
        /// </summary>
        /// <param name="inputrows">input rows</param>
        /// <returns>return IFizzBuzz List</returns>
        private List<IFizzBuzz> GetFizzBuzzList(int inputrows)
        {
            List<IFizzBuzz> list = new List<IFizzBuzz>();
            for (int index = 1; index <= inputrows; index++)
            {
                SimpleDivision objData = new SimpleDivision();
                objData.IsDivisible(index);
                list.Add(objData);
            }

            return list;
        }
    }
}
