﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzTests.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzz.Model;
    using FizzBuzzBusinessLayer;
    using NUnit.Framework;

    /// <summary>
    /// Definition of FizzBuzzTests class
    /// </summary>
    [TestFixture]
    public class FizzBuzzTests
    {
        /// <summary>
        /// if number is divisible by 3 and 5 then replace with proper message
        /// </summary>
        [Test]
        public void Input_Number_Replace_3_and_5_Divisible_With_Proper_Message()
        {
            IStrategic strategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = strategicLogic.GetList(24, System.DateTime.Now.DayOfWeek);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wizz Wuzz", ((IFizzBuzz)list[14]).Message);
            }
            else
            {
                Assert.AreEqual("Fizz Buzz", ((IFizzBuzz)list[14]).Message);
            }
        }
    }
}
