﻿//-----------------------------------------------------------------------
// <copyright file="BuzzTests.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzz.Model;
    using FizzBuzzBusinessLayer;
    using NUnit.Framework;

    /// <summary>
    /// Definition of BuzzTests class
    /// </summary>
    [TestFixture]
    public class BuzzTests
    {
        /// <summary>
        /// if number is divisible by 5 then replace with proper message
        /// </summary>
        [Test]
        public void Input_Number_Replace_5_Divisible_With_Proper_Message_Color()
        {
            IStrategic strategicLogic = new FizzBuzzStrategic();
            List<IFizzBuzz> list = strategicLogic.GetList(24, System.DateTime.Now.DayOfWeek);
            if (DateTime.Today.DayOfWeek == DayOfWeek.Wednesday)
            {
                Assert.AreEqual("Wuzz", ((IFizzBuzz)list[4]).Message);
                Assert.AreEqual("Wuzz", ((IFizzBuzz)list[9]).Message);
            }
            else
            {
                Assert.AreEqual("Buzz", ((IFizzBuzz)list[4]).Message);
                Assert.AreEqual("Buzz", ((IFizzBuzz)list[9]).Message);
            }

            Assert.AreEqual("green", ((IFizzBuzz)list[4]).MessageColor);
            Assert.AreEqual("green", ((IFizzBuzz)list[9]).MessageColor);
        }
    }
}
