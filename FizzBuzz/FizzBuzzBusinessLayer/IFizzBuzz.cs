﻿//-----------------------------------------------------------------------
// <copyright file="IFizzBuzz.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Abstraction of FizzBuzz
    /// </summary>
    public interface IFizzBuzz
    {
        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        string Message { get; }

        /// <summary>
        /// Gets or Sets MessageColor
        /// </summary>
        string MessageColor { get; }

        /// <summary>
        /// check whether number is divisible
        /// </summary>
        /// <param name="inputNumber">input number</param>
        /// <returns>return boolean</returns>
        bool IsDivisible(int inputNumber);

        /// <summary>
        /// to set message
        /// </summary>
        /// <param name="inputisSpecificDay">input IsSpecificDay</param>
        void SetMessage(bool inputisSpecificDay);
    }
}
