﻿//-----------------------------------------------------------------------
// <copyright file="Buzz.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// definition of Buzz class
    /// </summary>
    public class Buzz : IFizzBuzz
    {
        /// <summary>
        /// define sMessage
        /// </summary>
        private string inputMessage;

        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        public string Message
        {
            get { return this.inputMessage; }
        }

        /// <summary>
        /// Gets or Sets MessageColor
        /// </summary>
        public string MessageColor
        {
            get { return "green"; }
        }

        /// <summary>
        /// check whether number is divisible by 5
        /// </summary>
        /// <param name="inputNumber">the input Number</param>
        /// <returns> the boolean</returns>
        public bool IsDivisible(int inputNumber)
        {
            return (inputNumber % 5) == 0 ? true : false;
        }

        /// <summary>
        /// to set message
        /// </summary>
        /// <param name="isSpecificDay">the input IsSpecificDay</param>
        public void SetMessage(bool isSpecificDay)
        {
            this.inputMessage = isSpecificDay ? "Wuzz" : "Buzz";
        }
    }
}
