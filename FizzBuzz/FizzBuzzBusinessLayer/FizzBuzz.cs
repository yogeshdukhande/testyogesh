﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzz.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Definition of FizzBuzz class
    /// </summary>
    public class FizzBuzz : IFizzBuzz
    {
        /// <summary>
        /// Define SMessage
        /// </summary>
        private string inputMessage;

        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        public string Message
        {
            get { return this.inputMessage; }
        }

        /// <summary>
        /// Gets or Sets MessageColor
        /// </summary>
        public string MessageColor
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// check whether number is divisible by 3 and 5
        /// </summary>
        /// <param name="inputNumber">input Number</param>
        /// <returns>return boolean</returns>
        public bool IsDivisible(int inputNumber)
        {
            return ((inputNumber % 3) == 0 && (inputNumber % 5) == 0) ? true : false;
        }

        /// <summary>
        /// set message based on day
        /// </summary>
        /// <param name="inputisSpecificDay">input IsSpecificDay</param>
        public void SetMessage(bool inputisSpecificDay)
        {
            this.inputMessage = inputisSpecificDay ? "Wizz Wuzz" : "Fizz Buzz";
        }
    }
}
