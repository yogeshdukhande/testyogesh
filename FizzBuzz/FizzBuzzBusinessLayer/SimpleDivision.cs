﻿//-----------------------------------------------------------------------
// <copyright file="SimpleDivision.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Definition of SimpleDivision class
    /// </summary>
    public class SimpleDivision : IFizzBuzz
    {
        /// <summary>
        /// define sMessage
        /// </summary>
        private string inputMessage;

        /// <summary>
        /// Define number
        /// </summary>
        private int number;

        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        public string Message
        {
            get { return this.inputMessage; }
        }

        /// <summary>
        /// Gets or Sets MessageColor
        /// </summary>
        public string MessageColor
        {
            get { return string.Empty; }
        }

        /// <summary>
        /// to set message
        /// </summary>
        /// <param name="inputisSpecificDay">input IsSpecificDay</param>
        public void SetMessage(bool inputisSpecificDay)
        {
            this.inputMessage = this.number.ToString();
        }

        /// <summary>
        /// Check number is divisible
        /// </summary>
        /// <param name="inputNumber">input Number</param>
        /// <returns>return boolean</returns>
        public bool IsDivisible(int inputNumber)
        {
            this.number = inputNumber;
            return true;
        }
    }
}
