﻿//-----------------------------------------------------------------------
// <copyright file="Fizz.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// Definition of Fizz Class
    /// </summary>
    public class Fizz : IFizzBuzz
    {
        /// <summary>
        /// Define SMessage
        /// </summary>
        private string inputMessage;

        /// <summary>
        /// Gets or Sets Message
        /// </summary>
        public string Message
        {
            get { return this.inputMessage; }
        }

        /// <summary>
        /// Gets or Sets MessageColor
        /// </summary>
        public string MessageColor
        {
            get { return "blue"; }
        }

        /// <summary>
        /// check whether number is divisible by 3
        /// </summary>
        /// <param name="inputNumber">input inputNumber</param>
        /// <returns>return boolean</returns>
        public bool IsDivisible(int inputNumber)
        {
            return (inputNumber % 3) == 0 ? true : false;
        }

        /// <summary>
        /// set message based on specific day
        /// </summary>
        /// <param name="inputisSpecificDay">input IsSpecificDay</param>
        public void SetMessage(bool inputisSpecificDay)
        {
            this.inputMessage = inputisSpecificDay ? "Wizz" : "Fizz";
        }
    }
}
