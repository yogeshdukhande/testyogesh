﻿//-----------------------------------------------------------------------
// <copyright file="InstanceFactory.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzzBusinessLayer
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using StructureMap;

    /// <summary>
    /// Definition of InstanceFactory
    /// </summary>
    public class InstanceFactory
    {
        /// <summary>
        /// define namespace
        /// </summary>
        private static string clsnamespace = "FizzBuzzBusinessLayer.";

        /// <summary>
        /// Define array of classes
        /// </summary>
        private static string[] typeArry = { clsnamespace + "FizzBuzz", clsnamespace + "Fizz", clsnamespace + "Buzz", clsnamespace + "SimpleDivision" };

        /// <summary>
        /// get IFizzBuzz Instance
        /// </summary>
        /// <param name="inputisSpecificDay">input IsSpecificDay</param>
        /// <param name="inputnumber">Input number</param>
        /// <returns>return IFizzBuzz</returns>
        public static IFizzBuzz GetInstance(bool inputisSpecificDay, int inputnumber)
        {
            IFizzBuzz value = new SimpleDivision();
            foreach (string type in typeArry)
            {
                Type objType = Type.GetType(type);
                value = (IFizzBuzz)Activator.CreateInstance(objType);
                
                if (value.IsDivisible(inputnumber))
                {
                    value.SetMessage(inputisSpecificDay);
                    break;
                }
                else
                {
                    value = null;
                }
            }

            return value;
        }
    }
}
