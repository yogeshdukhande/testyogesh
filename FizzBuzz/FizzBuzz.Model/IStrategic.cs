﻿//-----------------------------------------------------------------------
// <copyright file="IStrategic.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzzBusinessLayer;

    /// <summary>
    /// Abstraction of Strategic logic
    /// </summary>
    public interface IStrategic
    {
        /// <summary>
        /// Gets the list of numbers from one up to the number given.
        /// </summary>
        /// <param name="inputMessage">The input number.</param>
        /// <param name="inputdayOfWeek">The input dayOfWeek.</param>
        /// <returns>List of IDivision</returns>
        List<IFizzBuzz> GetList(int inputMessage, DayOfWeek inputdayOfWeek);

        /// <summary>
        /// check is specific day
        /// </summary>
        /// <returns>return boolean</returns>
        bool IsSpecificDay();
    }
}
