﻿//-----------------------------------------------------------------------
// <copyright file="FizzBuzzStrategic.cs" company="Yogesh">
//     Copyright (c) Yogesh. All rights reserved.
// </copyright>
// <author>Yogesh Dukhande</author>
//-----------------------------------------------------------------------

namespace FizzBuzz.Model
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FizzBuzzBusinessLayer;

    /// <summary>
    /// The business logic of the application
    /// </summary>
    public class FizzBuzzStrategic : IStrategic
    {
        /// <summary>
        /// Field to specify day of week
        /// </summary>
        private DayOfWeek dayOfWeek;

        /// <summary>
        /// Gets the list of numbers from one up to the number given.
        /// </summary>
        /// <param name="inputnumber">The input number.</param>
        /// <param name="inputdayOfWeek">The input dayOfWeek.</param>
        /// <returns>List of numbers</returns>
        public List<IFizzBuzz> GetList(int inputnumber, DayOfWeek inputdayOfWeek)
        {
            List<IFizzBuzz> list = new List<IFizzBuzz>();
            this.dayOfWeek = inputdayOfWeek;
            for (var counter = 1; counter <= inputnumber; counter++)
            {
                IFizzBuzz obj = InstanceFactory.GetInstance(this.IsSpecificDay(), counter);
                if (obj != null)
                {
                    list.Add(obj);
                }
            }

            return list;
        }

        /// <summary>
        /// get whether today is wednesday
        /// </summary>
        /// <returns>return boolean</returns>
        public bool IsSpecificDay()
        {
            return DayOfWeek.Wednesday == this.dayOfWeek ? true : false;
        }
    }
}
